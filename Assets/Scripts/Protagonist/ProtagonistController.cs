﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtagonistController : MonoBehaviour
{
    // Get Component
    public GameObject Protagonist;
    public Rigidbody2D ProtagonistRigidBody;
    
    // Padding
    private float xMin;
    private float xMax;
    private float yMin;
    private float yMax;
    private float padding = 1;
    private Camera gameCamera;
    
    // Move
    [SerializeField] private float protagonistSpeed = 4;
    private Vector2 movementInput = Vector2.zero;
    private Vector2 inputDirection;
    private Vector2 inPutVelocity;

    private float newXpos;
    private float newYpos;

    // Create Bullet
    public Bullet PBullet;
    [SerializeField] public float fireRate;
    public float ThisTime;

    // Score
    public ScoreManager Score;
    
    // Materials
    public Material POriginalMaterial;
    public Material PFlashMaterial;
    public SpriteRenderer Renderer;
    [SerializeField]private float flashDuration;
    
    // Audio
    public AudioSource PFireSound;

    private void Awake()
    {
        ProtagonistRigidBody = Protagonist.GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        SetUpMoveBoundaries();
    }
    
    void Update()
    {
        Move();
        if (Input.GetKey(KeyCode.Space) && Time.time - ThisTime > fireRate)
        {
            ThisTime = Time.time;
            Attack();
        }
    }
    
    private void SetUpMoveBoundaries()
    {
        gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector2(0,0)).x+padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector2(1,0)).x-padding;
            
        yMin = gameCamera.ViewportToWorldPoint(new Vector2(0,0)).y+padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector2(0,1)).y-padding;
    }

    private void Move()
    { 
        movementInput = new Vector2(Input.GetAxisRaw("Horizontal"), 0);
        inputDirection = movementInput.normalized;
        inPutVelocity = inputDirection * protagonistSpeed;
         
        var newXPos = transform.position.x + inPutVelocity.x * Time.deltaTime;
        var newYPos = transform.position.y + inPutVelocity.y * Time.deltaTime;

        newXPos = Mathf.Clamp(newXPos,xMin,xMax);
            
        newYPos = Mathf.Clamp(newYPos, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);
    }

    private void Attack()
    {
        var bullet = Instantiate(this.PBullet);
        bullet.Score = Score;
        bullet.transform.position = transform.position;
        
        PFireSound.Play();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("EBullet"))
        {
            TakeHit();
        }
        else
        {
            OriginalMaterial();
        }
    }

    public void TakeHit()
    {
        Invoke("FlashMaterial",flashDuration);
        OriginalMaterial();
    }
    
    private void FlashMaterial()
    {
        Renderer.material = PFlashMaterial;
    }
    
    private void OriginalMaterial()
    {
        Renderer.material = POriginalMaterial;
    }
}
