﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreManager : MonoBehaviour
{
    public float FinalScore;
    public float Bulletscore;
    public float GetHitScore;

    public void AddScoreForBullet()
    {
        FinalScore += Bulletscore;
    }

    public void DeleteScoreForBullet()
    {
        FinalScore -= GetHitScore;
    }
}