﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Game UI Panel
    public GameObject StartPanel;
    public GameObject LevelSetUpPanel;
    public GameObject PausePanel;
    public GameObject FinalPanel;
    public GameObject PauseEmpty;
    
    // Characters
    public GameObject Protagonist;
    public GameObject Enemy;
    public ProtagonistController ProtagonistC;
    public EnemyController EnemyC;
    
    // Background
    public GameObject NormalBackground;
    public GameObject HardcoreBackground;

    // Timer
    public Text TimerText;
    [SerializeField] public float TimeRemaining = 60;

    // Score
    public ScoreManager PScoreManager;
    public Text ScoreText;
    public Text FinalScoreText;

    // Game status
    private bool isGameStart;
    private bool isGamePause;
    
    // Audio
    public AudioSource IntroAudio;

    private void Awake()
    {
        StartPanel.SetActive(true);
        LevelSetUpPanel.SetActive(false);
        PausePanel.SetActive(false);
        FinalPanel.SetActive(false);
        PauseEmpty.SetActive(false);
        Protagonist.SetActive(false);
        Enemy.SetActive(false);
        Time.timeScale = 0;
        isGameStart = false;
        isGamePause = true;
        
        IntroAudio.Play();
        IntroAudio.volume = 0.4f;//
    }

    void Start()
    {
        
    }

    public void FirstStart()
    {
        StartPanel.SetActive(false);
        LevelSetUpPanel.SetActive(true);
    }
    
    public void GameStart()
    {
        LevelSetUpPanel.SetActive(false);
        PauseEmpty.SetActive(true);
        Spawn();

        IntroAudio.volume = 0.2f;
    }

    public void NormalSetUp()
    {
        PScoreManager.Bulletscore = 5;
        PScoreManager.GetHitScore = 5;
        
        ProtagonistC.fireRate = 0.3f;
        EnemyC.fireRate = 0.4f;
        
        NormalBackground.SetActive(true);
        GameStart();
    }
    
    public void HardcoreSetUp()
    {
        PScoreManager.Bulletscore = 5;
        PScoreManager.GetHitScore = 10;

        ProtagonistC.fireRate = 0.3f;
        EnemyC.fireRate = 0.2f;
        
        HardcoreBackground.SetActive(true);
        GameStart();
    }
    
    public void QuitGame()
    {
        Application.Quit();
    }

    public void ResumeGame()
    {
        PauseEmpty.SetActive(true);
        PausePanel.SetActive(false);
        Time.timeScale = 1;
        isGamePause = false;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("Game");
    }

    public void PauseGame()
    {
        PauseEmpty.SetActive(false);
        PausePanel.SetActive(true);
        isGamePause = true;
        Time.timeScale = 0;
    }

    public void GameEnd(ScoreManager scoreManager)
    {
        FinalPanel.SetActive(true);
        FinalScoreText.text = scoreManager.FinalScore.ToString("0");
        isGamePause = true;
        Time.timeScale = 0;
        
        IntroAudio.volume = 0.4f;
        
        NormalBackground.SetActive(false);
        HardcoreBackground.SetActive(false);
    }

    //
    private void Spawn()
    {
        Protagonist.SetActive(true);
        Enemy.SetActive(true);
        Time.timeScale = 1;
        isGameStart = true;
        isGamePause = false;
    }

    public void Timer()
    {
        if (TimeRemaining > 0)
        {
            TimeRemaining -= Time.deltaTime;
            TimerText.text = TimeRemaining.ToString("0");
            
            if (TimeRemaining <=0)
            {
                TimeRemaining = 0;
                TimerText.text = TimeRemaining.ToString("0");
            }
        }
    }

    public void ScorePanel(ScoreManager scoreManager)
    {
        ScoreText.text = scoreManager.FinalScore.ToString("0");
    }

    void Update()
    {
        if (TimeRemaining > 0)
        {
            if (isGameStart == true && isGamePause == false)
            {
                Timer();
                ScorePanel(PScoreManager);
            }
        }

        else
        {
            GameEnd(PScoreManager);
        }
    }
}
