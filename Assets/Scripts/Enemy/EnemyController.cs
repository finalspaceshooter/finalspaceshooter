﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    // Move
    public Transform protagonistPosition;
    [SerializeField] private float enemySpeed = 2;

    // Create bullet
    public EBullet EBullet;
    [SerializeField] public float fireRate;
    
    // Score
    public ScoreManager Score;

    // Materials for flashing
    public Material EOriginalMaterial;
    public Material EFlashMaterial;
    public SpriteRenderer Renderer;
    [SerializeField] public float flashDuration;

    // Audio
    public AudioSource EFireSound;

    void Start()
    {
        InvokeRepeating("Attack",1,fireRate);
        Renderer.material = EOriginalMaterial;
    }
    
    void Update()
    {
        Move();
    }

    void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position,
            new Vector2(protagonistPosition.position.x, transform.position.y), enemySpeed * Time.deltaTime);
    }

    private void Attack()
    {
        var bullet = Instantiate(this.EBullet);
        bullet.Score = Score;
        bullet.transform.position = transform.position;
        EFireSound.Play();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("PBullet"))
        {
            TakeHit();
        }
        else
        {
            OriginalMaterial();
        }
    }

    public void TakeHit()
    {
        Invoke("FlashMaterial",flashDuration);
        OriginalMaterial();
    }
    
    private void FlashMaterial()
    {
        Renderer.material = EFlashMaterial;
    }
    
    private void OriginalMaterial()
    {
        Renderer.material = EOriginalMaterial;
    }
}